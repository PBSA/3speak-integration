#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string, random
from beem.nodelist import NodeList
from beem import Hive
import beem
import pymongo
import json
import requests
# from graphenebase import account
# from peerplaysbase import account
from peerplaysbase.account import PasswordKey, PublicKey
 
import peerplays

# wif = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3" # irona
wif = "5Kbq6V2g1HRq4Wp6pcdMiTb7VHC79UJXFJCfpSjZH6h8fngwuyN" #ymir

wif = ["5Kbq6V2g1HRq4Wp6pcdMiTb7VHC79UJXFJCfpSjZH6h8fngwuyN", #ymir
       "5JmeDQjrxByazSSKRn9MdUk8kA7TL132YYr1uTmHQSbW5cR52bD"]
# node = "ws://96.46.48.104:8090/api"
# node = "wss://irona.peerplays.download/api"
node = "ws://96.46.48.103:8090"
urlFaucet = "https://irona-faucet.peerplays.download/api/v1/accounts"
p = peerplays.PeerPlays(node=node, prefix="TEST", wif=wif)
p.unlock("gnulinux")

def AccountPeerplays(name):
    account = p.rpc.get_account(name)
    return account

def get_hive_nodes():
    nodelist = NodeList()
    nodes = nodelist.get_hive_nodes()
    nodelist.update_nodes(blockchain_instance=Hive(node=nodes, num_retries=10))
    return nodelist.get_hive_nodes()
    #return "https://beta.openhive.network"


bts = Hive(
    node=get_hive_nodes(),
    nobroadcast=True,
    # keys={"active": wif},
    num_retires=10
    )

def Authenticate(accountName, postingKeyPvt):
    # return True
    # account = beem.account.Account(accountName)
    # try:
    account = beem.account.Account(accountName)
    postingKey = account["posting"]["key_auths"][0][0]
    # memoKey = account["memo_key"]
    #pvt = beem.wallet.PrivateKey(memoKeyPvt)
    try:
        pvt = beem.wallet.PrivateKey(postingKeyPvt)
    except:
        print("WIF not valid")
        return False
    pubKey = str(pvt.get_public_key())
    # if pubKey == memoKey:
    if pubKey == postingKey:
        return True
    return False
    # except:
    return False

    return True
    try:
        account = beem.account.Account(accountName)
        memoKey = account["memo_key"]
        pvt = beem.wallet.PrivateKey(memoKeyPvt)
        pubKey = str(pvt.get_public_key())
        if pubKey == memoKey:
            return True
        return True
    except:
        return True

def Account(accountName):
    # account = {"a": "a", "b":"b"}
    # return account
    # account = beem.account.Account(str(accountName))
    try:
        account = beem.account.Account(str(accountName))
    except:
        return None
    # account = beem.account.Account(accountName)
    account = json.loads(json.dumps(dict(account), default=str))
    return account

# 95.216.90.243

# I want to delete this file

def AccountCreate(accountName):
    # urlFaucet = "https://irona-faucet.peerplays.download/api/v1/accounts"
    # urlFaucet = "http://96.46.48.104:5000/api/v1/accounts" 
    # urlFaucet = "http://192.168.20.67:5000/api/v1/accounts" 
    account_name = accountName
    password = accountName

    active_key = PasswordKey(account_name, password, role="active", prefix="TEST")
    owner_key = PasswordKey(account_name, password, role="owner", prefix="TEST")
    memo_key = PasswordKey(account_name, password, role="memo", prefix="TEST")
    active_pubkey = active_key.get_public_key()
    owner_pubkey = owner_key.get_public_key()
    memo_pubkey = memo_key.get_public_key()
    active_privkey = active_key.get_private_key()
    owner_privkey   = owner_key.get_private_key()
    memo_privkey = memo_key.get_private_key()
    str_active_pubkey = str(owner_pubkey)
    str_owner_pubkey = str(owner_pubkey)
    str_memo_pubkey = str(memo_pubkey)

    keys = dict()
    keys['owner_key'] = owner_privkey
    keys['active_key'] = active_privkey
    keys['memo_key'] = memo_privkey

    j = {'account': {
        'name': 'qa3',
#        'owner_key': owner_key,
#        'active_key': active_key,
#        'memo_key': memo_key,
        'owner_key': str_owner_pubkey,
        'active_key': str_active_pubkey,
        'memo_key': str_memo_pubkey,
        # 'password': password,
        'refcode': '',
        'referrer': ''}}
    j["account"]["name"] = accountName
    print("j:", j)
    r = requests.post(urlFaucet, json=j)
    return r, keys # , j

def Claim(accountNameHive, amount):
    # accountPeerPlays = p.rpc.get_account(accountNameHive)
    # if isinstance(accountPeerPlays, type(None)):
    #     accountNamePeerPlays = accountNameHive
    # else:
        # accountNamePeerPlays = "3SPEAK." + accountNameHive 
    accountNamePeerPlays = "threespeak." + accountNameHive + ''.join(random.choices(string.digits, k=10))
    r, keys = AccountCreate(accountNamePeerPlays) 
    p.transfer(accountNamePeerPlays, amount, "TEST", memo="Speak Calimdrop", account="nathan") 
    print("r.text:", r.text) 
    print("keys:", keys)
    print("accountnamePP:", accountNamePeerPlays)
    return r, keys, accountNamePeerPlays

def ClaimPU(accountNamePeerPlays, amount):
    # accountPeerPlays = p.rpc.get_account(accountNameHive)
    # if isinstance(accountPeerPlays, type(None)):
    #     accountNamePeerPlays = accountNameHive
    # else:
        # accountNamePeerPlays = "3SPEAK." + accountNameHive 
    # accountNamePeerPlays = "threespeak." + accountNameHive + ''.join(random.choices(string.digits, k=10))
    # r, keys = AccountCreate(accountNamePeerPlays) 
    p.transfer(accountNamePeerPlays, amount, "LARYNX", memo="Speak Calimdrop", account="spk-treasury") 
    # print("r.text:", r.text) 
    # print("keys:", keys)
    print("accountnamePP:", accountNamePeerPlays)
    keys = dict() 
    r = dict()
    return r, keys, accountNamePeerPlays

def PeerplaysAccountCreate(accountNameHive):
    accountPeerPlays = p.rpc.get_account(accountNameHive)
    # if isinstance(accountPeerPlays, type(None)):
        # accountNamePeerPlays = accountNameHive
    # else:
    accountNamePeerPlays = "3SPEAK." + accountNameHive 
    accountNamePeerPlays = "threespeak." + accountNameHive + ''.join(random.choices(string.digits, k=10))
    r, keys = AccountCreate(accountNamePeerPlays) 
    # p.transfer(accountNamePeerPlays, amount, "LARNYX", memo="Speak Calimdrop", account="nathan") 
    print("r.text:", r.text) 
    print("keys:", keys)
    print("accountnamePP:", accountNamePeerPlays)
    return r, keys, accountNamePeerPlays
