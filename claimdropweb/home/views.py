import json
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .forms import UserForm , UserRegistrationForm
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib import messages
# from CaseInsensitiveDict import CaseInsensitiveDict
from django.http import HttpResponse
from rest_framework.decorators import api_view
from django.http import JsonResponse

import time

from .authenticate import Authenticate, Account, Claim, ClaimPU, AccountPeerplays, PeerplaysAccountCreate
from .snapshot import Snapshot

snapshotObject = Snapshot()

@login_required
def index(request):
    account = Account(request.user)
    privateKeys = {}
    amount = 0
    if request.method == "POST":
        claimedUser = snapshotObject.colClaimStatuses.find_one({"hive_username":str(request.user)})
        print("claimedUser:", claimedUser)
        if type(claimedUser) != type(None):
            return HttpResponse("Larnyx already claimed by the user " + str(request.user))
        username = str(request.user)
        larnyx2bClaimed, balanceHive, balanceHivePower  = snapshotObject.HiveUser(username)  # snapshotObjet.HiveUser(username)
        r, privateKeys, accountNamePeerPlays = Claim(str(request.user), larnyx2bClaimed)
        # print("keys:", keys)
        # time.sleep(5)
        account = AccountPeerplays(accountNamePeerPlays)
        account["hive_username"] = str(request.user)
        # print("typeSnapshot:", type(snapshot))
        snapshotObject.colClaimStatuses.insert_one(account)
        print("account:", account)

    return render(request, 'index.html', {"account":account, "privateKeys":privateKeys})
    # return render(request, 'index.html', {})
    
def signin(request):
    '''
    Sign into the application.
    '''

    if request.method == "POST":
        username = request.POST['username']
        password =  request.POST['password']
        user = api_login(username , password)

        if user:
            user_inapp = check_user_application(username , password)
            
            if user_inapp is None:
                user_registration_form = UserRegistrationForm(request.POST)
                if user_registration_form.is_valid():
                    user_registration_form.save()
                    messages.success(request, 'Your profile was successfully updated!')
                    authenticate_login(request , username , password)
                    return redirect('/')
            else:
                authenticate_login(request , username , password)
                return redirect('/index/')
                
        else:
            titleMessage = "Invalid Credentials, Try again!"
            form = UserForm()
            return render(request, 'login.html', {'form':form, "titleMessage":titleMessage})
            return HttpResponse("Invalid credentials.")
        
    form = UserForm()
    titleMessage = "Login to Claim LARNYX"
    return render(request, 'login.html', {'form':form, "titleMessage":titleMessage})

def authenticate_login(request , username , password):
    '''
    Authenticate login using the username and password
    '''
    user = authenticate(
                        request, 
                        username=username, 
                        password=password
                )

    
    if user is not None:
        login(request, user)

def check_user_application(username , password):
    '''
    Check user in the application.
    '''
    user_details = User.objects.filter(username=username).first()
    return user_details

def api_login(username , password):
    '''
    This is the function to third part call
    '''


    return Authenticate(username, password)

    if username and password:
        return True
    else:
        return False

def snapshot(request):
    signout(request)
    # snapshotObject = Snapshot()
    # print("snapshotObject:", snapshotObject)
    # account = Account(request.user)
    amount = 0
    # print("request:", request.keys())
    if request.method == "POST":
        username = request.POST["username"]
        print("username from POST:", username)
        print("username from request:", str(request.user))
        claimedUser = snapshotObject.colClaimStatuses.find_one({"hive_username":username})
        print("claimedUser:", claimedUser)
        if type(claimedUser) != type(None):
            hiveMessage = "Larynx already claimed by the user " + str(username)
            # hiveMessage = "Hive at snapshot for the user " + str(username) + " is " + str(balanceHive)
            # hivePowerMessage = "HivePower at snapshot for the user " + str(username) + " is " + str(balanceHivePower)
            # userLarynxMessage = "Larynx to be claimed for the user " + str(username) + " is " + str(larynx2bClaimed)
            # return render(request, 'snapshot.html', {"totalSupply": "462204528.7846216", "hiveMessage":hiveMessage, "hivePowerMessage":hivePowerMessage, "userLarynxMessage":userLarynxMessage})
            return render(request, 'snapshot.html', {"totalSupply": "462204528.7846216", "hiveMessage":hiveMessage}) # , "hivePowerMessage":hivePowerMessage, "userLarynxMessage":userLarynxMessage})
        try:
            print("try starting")
            larynx2bClaimed, balanceHive, balanceHivePower  = snapshotObject.HiveUser(username)  # snapshotObjet.HiveUser(username)
            print("larynx2bClaimed, balanceHive, balanceHivePower:  ", larynx2bClaimed)
            hiveMessage = "Hive at snapshot for the user " + str(username) + " is " + str(balanceHive)
            hivePowerMessage = "HivePower at snapshot for the user " + str(username) + " is " + str(balanceHivePower)
            userLarynxMessage = "Larynx to be claimed for the user " + str(username) + " is " + str(larynx2bClaimed)
        except:
            userLarynxMessage = "User doesn't exist!"
            hiveMessage = ""
            hivePowerMessage = ""
        return render(request, 'snapshot.html', {"totalSupply": "462204528.7846216", "hiveMessage":hiveMessage, "hivePowerMessage":hivePowerMessage, "userLarynxMessage":userLarynxMessage})

    return render(request, 'snapshot.html', {"totalSupply": "462204528.7846216"})


def signout(request):
    '''
    Sign Out functionality
    '''
    logout(request)
    return redirect('/snapshot/')

@api_view(['GET', 'POST'])
def peerplays_create_account(request):
    if request.method == 'GET':
        hiveUsername = request.GET.get('hu')
        print("hiveUsername:", hiveUsername)
        r, keys, accountNamePeerplays = PeerplaysAccountCreate(hiveUsername)
        res = dict()

        keys = json.loads(json.dumps(str(keys)))
        res["keys"] = keys
        res["PeerplaysAccountName"] = str(accountNamePeerplays)
        print("account created:", r, keys, accountNamePeerplays)
        return JsonResponse(res)
    return JsonResponse({'error':'Peerplays Account Creation, didnt complete weel'})


@api_view(['GET','POST'])
def snap(request):
    """
    List all get parameters 
    sample usage: http://0.0.0.0:8000/api/snapshot?u=henry
    """
    if request.method == 'GET':
        username = request.GET.get('u')
        print("username:", username)
        resSnap = snapshotObject.ToBeClaimedReverseTemp(username)
        print("done:", resSnap)
        # class_obj = request.GET.get('cls')
        # division = request.GET.get('div')
        # return JsonResponse({'username':username, 'hive':balHive})
        return JsonResponse(resSnap)
    elif request.method == "POST":
        data = request.data
        username = request.data["u"]
        resData = snapshotObject.ClaimRegister(username)
        print("POST username:", username)
        return JsonResponse(resData)
        # return JsonResponse({"username":username, "message":"Claim request registered"})
    else:
        return JsonResponse({'error':'Error'})

"""
$ curl -i -X POST -H 'Content-Type: application/json' -d '{"u": "henry"}' http://96.46.48.108:8002/api/snapshot?u=henry

"""
    
@api_view(['GET','POST'])
def claim(request):
    """
    List all get parameters 
    sample usage: http://0.0.0.0:8000/api/snapshot?u=henry
    """
    if request.method == 'GET':
        print ("reuest is:", request)
        hiveUsername = request.GET.get("hu")
        print ("hu:", hiveUsername)
        amountLarynx = request.GET.get("al")
        print ("al:", amountLarynx)
        r, privateKeys, accountNamePeerPlays = Claim(hiveUsername, amountLarynx)
        resDict = dict()
        resDict["privateKeys"] = str(privateKeys)
        resDict["accountNamePeerPlays"] = accountNamePeerPlays
        return JsonResponse(resDict)
    else:
        return JsonResponse({'error':'Error'})

@api_view(['GET','POST'])
def claim_pu(request):
    """
    List all get parameters 
    sample usage: http://0.0.0.0:8000/api/snapshot?u=henry
    """
    if request.method == 'GET':
        print ("reuest is:", request)
        peerplaysUsername = request.GET.get("pu")
        print ("pu:", peerplaysUsername)
        amountLarynx = request.GET.get("al")
        print ("al:", amountLarynx)
        r, privateKeys, accountNamePeerPlays = ClaimPU(peerplaysUsername, amountLarynx)
        resDict = dict()
        resDict["privateKeys"] = str(privateKeys)
        resDict["accountNamePeerPlays"] = accountNamePeerPlays
        return JsonResponse(resDict)
    else:
        return JsonResponse({'error':'Error'})

