# SPK Network Claim Drop Backend

## Installation Guide

### 1. Infrastructure Requirements
The infrastructure requirement depends on the peak load on the sanpshot API.
A multi core processor machine with 16 GB RAM, 1 TB HDD is recommended.

A load balancer like Nginx is recommended to manage high volume traffic.

### 2. Prerequisites

The below prerequisite dependencies should be installed (Ubuntu 18.04 or higher OS version):
```
apt update
apt install build-essential libssl-dev libffi-dev python3.8 python3.8-dev virtualenv
```


### 3. Deployment Steps
#### 3.1 Manual deployment

Clone this repo
```
git clone https://gitlab.com/PBSA/3speak-integration
```
If cloning a particular branch then:
```
git clone https://gitlab.com/PBSA/3speak-integration -b <branch-name>
```

Goto to the directory
```
cd 3speak-integration/claimdropweb/
```

Run the shell script 
```
bash deploy.sh <address:port>
```
The address and port are the ones at which this application would be accessible. The possible options for the address here are:
1. 127.0.0.1:<port>
2. 0.0.0.0:<port>
3. localhost:<port>

The address here should be the IP address of the any interface on which the server is being deployed. 

After successful execution of the script the application should be available at the `http:/<address:port>/` 

#### 3.2 Docker deployment

Clone this repo
```
git clone https://gitlab.com/PBSA/3speak-integration
```
If cloning a particular branch then:
```
git clone https://gitlab.com/PBSA/3speak-integration -b <branch-name>
```

Goto to the directory
```
cd 3speak-integration/
```

Build the docker image
```
docker build -f Dockerfile -t <tag_name> .
```

Run the docker image
```
docker run -p 8000:8000 -d <tag_name>
```

The application will be available at http:/<youripaddress>:8000

### 4. DB and Block Height 
Configrure URL and credentials of DB at line number 63 of ``` claimdropweb/home/snapshot.py ```

Block height of snapshot shall be configured at line number 34 of ``` claimdropweb/home/snapshot.py ```





